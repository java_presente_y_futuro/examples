# Java moderno y futuro
## Presentación

Inicié este sencillo proyecto para enseñar a los nuevos programadores (y también a los experimentados) las nuevas características del lenguaje de programación Java, así como los nuevos paradigmas de programación que los diseñadores del lenguaje en Oracle recomiendan usar.

El motivo de esto es que siento que la manera de enseñar Java ha quedado obsoleta. Aún se enseña Java como si estuviéramos en el año 2007, utilizando los estilos de programación de Java 7 o anteriores; empleando clases obsoletas que solo se mantienen por razones de retrocompatibilidad. Esto complica más de lo necesario tareas sencillas con enfoques de trabajo que los mismos desarrolladores de Java desaconsejan. En general, todo esto da la sensación de que Java es un lenguaje viejo, obsoleto, complicado y aburrido, excesivamente verboso y ceremonioso. Sin embargo, esto no es cierto desde hace años, y sigue mejorando. Cada semestre, las nuevas versiones de Java se expanden y se vuelven más fáciles y sencillas.

En este repositorio, que iré expandiendo poco a poco, se usarán y abusarán de características que pueden estar en preview. Si deseas probar las soluciones aquí presentadas, debes configurar tu IDE para lidiar con esto.


## Entorno de desarrollo
* SO: Fedora 40
* SDK: OpenJDK 22
* IDE: IntelliJ Ultimate

## Estilos de programación:
Dado que se busca mostrar las caracteristicas recientes de Java se preferirán los siguientes estilos de programación:
* Se usará un estilo de programación funcional cuando sea posible, por sobre su alternativa declarativa. Esto debido a que suele resultar en codigo más expresivo y corto
* Se usará [programación orientada a datos](https://www.infoq.com/articles/data-oriented-programming-java/) (DOP) por sobre programación orientada a objetos (OOP) cuando sea posible. Ya que es un estilo de programación más acorde con las arquitecturas y practicas actuales basadas en microservicios
* Solo se usarán expresiones lambda (a -> {}) solo si la expresión es menor a 3 lineas de codigo, más allá de esto la expresión se extraerá en una funsión.
* Primarán patrones de diseño sencillos de elaborar por sobre complejos, se seguirá el principio KISS (Keep it simple, stupid)
* Los retos de programación solo contendran la solución general, debido que el objetivo es mostrar las nuevas funciones de Java, no se presentarán soluciones a estos retos que contengan todas las comprobaciones y salvaguardas a los "casos borde", salvo que estas sirvan para mostrar una nueva o poco usada/enseñada caracteristica de Java.