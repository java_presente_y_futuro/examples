/*
La api stream nos permite operar con colecciones y con arreglos de manera sencilla y con un estilo funcional, concatenando operaciones
La api stream requere del 2 conceptos fundamentales para su aplicación: Interfaces funcionales y los tipos de operaciones.
 */

import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

List<Integer> numberList = List.of(1,5,8,7,4,6,9,8,5,32,1,4,7,-85,-95,57,584,5226,-635,-55,8528,-14,114);
List<MyUser> listOfUsers = List.of(
        new MyUser("Ewig",30),
        new MyUser("Crystal", 28),
        new MyUser("Alarik",34),
        new MyUser("Luric", 42)
);
void main(){

    // INTERFACES FUNCIONALES
    /*Cuando miramos que hay dentro de la interfaz Stream nos damos cuenta que esta llena de metodos que toman como
    paraemtros de entrada mayormente interfaces funcionales. En java existen 6 interfaces funcionales principales
    1) Consumers: tienen parametros de entrada pero no devuelven ningun valor void foo(T t)
    2) Producers: No tienen parametros de entrada y devuelven un valor T foo()
    3) functionss: Toman un parametro y devuelven un valor R foo(T t)
    4) Predicates: toman una expresión y devuelven un valor booleano. boolean foo(T t)
    5) Collectors: Interfaz funcional de java.Collections que sirve para agrupar objetos en una collección
    6) Comparators: Interfaz funcional de Java.Collections que sirve para ordenar elementos según un criterio estabelcido

    Entender y estudiar estas interfaces funcionales es indispensable para sacar mayor provecho de los streams de Java.
    * */

    //TIPOS DE OPERACIONES
    /*
    En los Streams the Java exiten 2 clases de metodos u operaciones. Las terminales y intermedias.
    Las operaciones intermedias son aquellas que devuelven como resultado un stream mientras que las
    terminales son aquellas que devuelven cualquier otro valor (incluyendo ningun valor, como un void)
    Solo puede haber una sola operación terminal por stream.
     */
    Stream<Integer> stream = numberList.stream();// operación intermedia
    long count = stream.count(); // operación terminal, devuelve un Long desde el stream
    System.out.println("The number of elements is: "+count);
    /*
    Una caracteristica interesante de los Streams es que son "Lazy" es decir, si no estan asociados a una operación terminal
    entonces no se ejecutan en absoluto. esto permite optimizar el numero de isntrucciones en tiempo de ejecución.
    por ejemplo
     */
    Stream<Integer> stream2 = numberList.stream();
    /*Otro rasgo interesante de los streams es que son "auto disposable"
    * es decir que se eliminan solos de memoria una vez han sido "cosnumidos" por una operación terminal
    * esto optimiza espacio en memoria y mejora la integridad de datos ya que solo puede operarse sobre copias
    * Esto mejora la integridad de los datos, pero hacer una gran cantidad de copias tiene costo a nivel de rendimiento
     */

    var console = System.console();
    if(!console.readLine().isEmpty()){
        System.out.println(stream2.count());
    }
    //Descomentar estas funciones para probar cada una
    //consumersShowcase(numberList);
    //producerShowcase();
    //predicateShowcase();
    //comparatorShowcase(numberList, listOfUsers);
    collectorsShowcase(numberList, listOfUsers);

}
private static void consumersShowcase(List<Integer> numbers){
    //1) Consumer: El consumer es una función que toma algo y no devuelve nada. la operación más usada que requeire un consumer es "forEach"
    numbers.stream().forEach( x -> System.out.println(x*2));
    System.out.println("///////////");
    numbers.stream()
            .peek(System.out::println) //Peek es un consumer similar a foreach pero sin temrinar el stream. muy usado para hacer debbug de operaciones intermedias
            .map(n -> n*2)
            .forEach(System.out::println);
}

private static void producerShowcase(){
    IntStream.iterate(0,x -> x+2)//IntStream es una clase estatica que genera un flujo de enteros.
            .limit(10)// Este stream imita hasta cierto punto el for i in range(0,20,2) de python
            .forEach(System.out::println);
}
private static void functionShowcase(){
    IntStream.iterate(0,x -> x+2)
            .limit(10)
            .mapToDouble(Math::sqrt) // map y todas sus variables implementan como parametro la interfaz funcional Function
            .forEach(System.out::println);
}
private static void predicateShowcase(){
    var multipleOfFive = IntStream.rangeClosed(0,20)
            .filter(n -> n%2!=0)// filter usa Predicates, solo permite pasar al siguiente paso de la cadena de operaciones que hacen que la condición sea verdadera
            .anyMatch(n -> n%5==0);// predicado que indica si cualquiera de los numeros que son impares y además multiplos de 5. esta es una operación terminal.
    System.out.println(multipleOfFive);
}
private static void comparatorShowcase(List<Integer>numberList, List<MyUser> listOfUsers){
    numberList.stream().sorted().forEach(System.out::println);// sorted es el metodo con comparadores más usado.
    // tambien existe la posbilidad de crear comparadores personalizados mediante funciones lamda, ya que un Comparator es una interfaz funcional.

    listOfUsers.stream().sorted(Comparator.comparing(MyUser::name)).forEach(System.out::println); //Ordenamiento por orden alfabetico del nombre
}
private static void collectorsShowcase(List<Integer> numbers, List<MyUser>listOfUsers){
    // Los collectores se encargan de convertir el resultado del stream a una colleción (List, set, map. Queue, etc) y Arrays segun distintos criterios estabelcidos por el programador.

    // convertir a set
    var numSet = numbers.stream()
            .filter(x -> x>=0)
            .collect(Collectors.toSet());
    numSet.forEach(System.out::println);

    //Convertir a un HashMap, agrupando quienes tengan menos de 30 años del resto
    var clasifiedByAge = listOfUsers.stream().collect(Collectors.partitioningBy(user -> user.edad <30));
    clasifiedByAge.forEach((k,v) -> {
        if(k) System.out.println("menores de  30: "+v);
        else System.out.println("Mayores de  30: "+v);
    });
}

private record MyUser(String name, int edad){}

