import java.util.function.Function;

void main(){
    //2) llamamos nuestra función y pasamos el callback como una lambda.
    var result = myFunctionWithCallback(20, x ->x*10);
    System.out.println(result);
    System.out.println(myFucntionWithCallback2(15,x ->x-1));
}
/*/
Un Callback es una función que se ejecuta dentro de otra al pasarla como argumento.
Esta funcionalidad, muy usada en javascript, puede ser imitada en java usando programación funcional.
 */

//1) lo primero es definir una función con sus argumentos, asegurandonos de que uno de ellos es una interfaz Fucntion
private static int myFunctionWithCallback(int numA, Function<Integer,Integer> callback){

    return callback.apply(numA);
}

//3) En Java existen 4 interfaces funcionales basicas: Function, Consumer, producer y predicate. Una interfaz funcional  es aquella que solo tiene un metodo abstracto.
// Las interfaces funcionales cuentan con metodos para concatenar operaciones. por ejemplo esta "andThen"

private static String myFucntionWithCallback2(int numA, Function<Integer,Integer> callback){

    return callback
            .andThen(x ->{
                System.out.println("and then 1");
                System.out.println(x);
                return String.valueOf(x-5);//Se suma antes de convertirse en String
                })
            .andThen(x ->{
                System.out.println("andThen 2");
                System.out.println(x);
                return x+3;// aquí se concatenan como si fueran string
            })
            .andThen(x ->{
                System.out.println("andThen 3");
                System.out.println(x);
                return String.valueOf(Integer.parseInt(x)+7);
            })
            .apply(numA);
    //En este caso primero restamos 5 a numA y luego lo volvemos un String. x = numA, se pueden encadenar tantos andThen como se desee.
    // primero se ejecuta la función callback en apply y luego los andThen. puede ser confuso ya que se lee "al reves"

}

