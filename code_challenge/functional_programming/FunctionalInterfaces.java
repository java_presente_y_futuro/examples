void  main(){
    /*
    Las interfaces funcionales nos permiten definir mediante el operador flecha
    la implementación del metodo abstracto en una clase anonima.
     */
   MyFunctionalInterface functionalInterface = x -> x+10;
   var result = functionalInterface.run(10);//Ejecutamos el metodo no abstracto dentro de la interfaz con la clase anonima creada.
    System.out.println(result);

    System.out.println(callback(75, x -> x-23));// Tambien podemos usar el metodo abstracto para crear callbacks

}
private static int callback(int num, MyFunctionalInterface myFunctionalInterface){
    return myFunctionalInterface.run(5);
}

/*
Una interfaz funcional es aquella que solo tiene un metodo abstracto
un metodo abstracto se define como aquel metodo que no tiene un comportamiento
definido, similar a los prototipos de función que se ponen en los headers de C/C++
 */
@FunctionalInterface //La anotación functional interface es opcional y solo con fines de documentación.
private interface MyFunctionalInterface{
    int printNum(int x);
    // Las interfaces funcionales pueden tener todos los metodos definidos que se quiera
    // para definir el comportamiento por defecto de un metodo en una interfaz usamos default
    default int run(int x){
       return printNum(x*20);
    }
}