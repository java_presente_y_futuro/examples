void main(){
//1) Cuando se declara la isntancia de la clase con el generico este debe especificarse
    var myClass = new MyClass<Integer>();// Solo las clases pueden ser asignados a tipos genericos, si intentamos hacerlo con primitivos tendremos un error de compilación.
    //var myAnotherClass = new MyClass<int>(); error
    myClass.setMyField(20);//Como ya especificamos el tipo generico como Integer, si ponemos otro tipo de dato dará error
    //myClass.setMyField(20.5); error
    System.out.println(myClass.getMyField());

    // 2) Los genericos tambien pueden aplicarse a interfaces funcionales mediante funciones lamda
    MyInterface<Integer, String> myInterface = x -> Integer.toString(x);
    var result = myInterface.myFunc(20);
    System.out.println(result + 3);
    // puede verse como el metodo abstracto devuelve acepta un String por que así se definio en los genericos de la interfaz funcional.
    //La sintaxis es similar a la usada para crear listas, mapas y sets, esto es por que dichas clases son genericas. Usando genericos podria de manera muy sencilla crear una implementación propia de Java Collections



}
/*
Los genericos son links simbolicos de una clase que cambiar por el tipo requerido en tiempo de compilación
en este caso queremos decir que dentro de la clase usaremos metodos y propeidades del tipo T
este tipo se especificará cuando la clase sea isntanciada.
 */

// Para usar estaticos en Java lo primero es crear una clase/interfaz con los la etiqueta generica entre el operador diamante <>
private static class MyClass<T>{
    private T myField; // podemos declarar propeidades del tipo generico.
    public T getMyField(){ // podemos especificar el tipo generico como el resultado de un metodo, así como el tipo del parametro de entrada.
        return this.myField;
    }
    public void setMyField(T t){
        this.myField = t;
    }
}
@FunctionalInterface
private interface MyInterface <T, R>{ //Una clase/Interfaz puede tener la cantidad de datos genericos que se desee
    R myFunc(T t);
}
