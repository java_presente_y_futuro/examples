/*
Las sealed classes o clases selladas en java son parte de lo que en Oracle denominan "Data Oriented programming"
o "Programación orientada a datos" La programación orientada a datos se refiere a un paradigma de programación en
el cual lo que importa no son es la definición de objetos, comportamientos y el estado de los mismos, sino que ahce
enfasis en usar a los objetos como portadores simples de datos, de manera similar a los Structs en C o los Objetos de JS.
En el caso de java su equivalente serían los records. la programación orientada a datos se basa en 2 pilares fundamentales.

A) Asegurar la integridad de los datos
B) Asegurar la eficiencia del consumo de los datos

Para lograr esto se usan 2 estrategias principales, lo primero es que los datos sean transportados desde su origen a su
destino por "data carriers" inmutables, de modo tal que los datos originales no puedan ser modificados directamente,
solo copiados. La segunda medida es la exhaustividad. la exhaustividad no solo permite que el codigo y por ende los datos sean más
predecibles al asegurarse de darle manejo a todos los posibles escenarios (incluyendo fallas o excepciones) sino que
además permiten a los compiladores (En este caso el HotSpot de la JVM) hacer optimizaciones muy fuertes debajo de la mesa.

Las clases selladas permiten limitar una caracteristica importante de la POO: la herencia.
una clase sellada puede explicitar que clases pueden heredar de ella (o implementarla si es una interfaz)
esto permite que el codigo sea más predecible al evitar que, por ejemplo, terceros hereden clases de librerias y SDKs
que no estan pensadas para ser extendidas. Anteriormente para hacer esto debia ponerse a cada clase el modificador "final"
una clase final es una clase que no puede ser heredada, las clases/interfaces selladas son una versión refinada
 y más flexible del mismo concepto
 */
void main(){

    /**
    2) Sin embargo el poder limitar la herencia para tener un codigo más organizado, seguro y predecible no es la unica
    ventaja de las clases selladas, ya que esto es algo que ocupa mayormente a quienes desarrollan APIs, librerias y SDKs.

     Otra ventaja, quizás la más útil para nuestro el día a dia, es su aplicación en la programación orientada a Datos.
     Las clases selladas permiten que la expresión switch-case haga evaluaciones exhaustivas, es decir no se requiere usar
     un caso "default". Esto permite a la JVM hacer bastantes optimizaciones debajo de la mesa y además asegura que en
     caso de extender la funcionalidad de un programa o de agregar más clases permitidad en la itnerfaz, el compilador no
     nos dejará compilar a menos que demos manejo a este nuevo caso. Esto mejora la extensibilidad del codigo en el
     largo plazo.
     */
    GeometricFigure figure = new Square(10, 20);
    switch (figure){
        case Circle circle -> System.out.println("Es un circulo");
        case Square square -> System.out.println("Es un cuadrado");
    }

}

/*1 ) Si tratamos de implementar/extender una clase o interfaz sellada y no estamos en su "lista blanca" el compilador
nos dira que tenemos un error
* */
private record Square  (int lenX, int leny) implements GeometricFigure{}
private record Circle (int radius)implements GeometricFigure{}
private sealed interface GeometricFigure permits Square, Circle{
}
//private record Star(int sides) implements GeometricFigure{}// Comentar esta linea para compilar
