/*En Java pattern matching es una caracteristica del lengiaje que permite evaluar si un determinado objeto es de una clase especifica,
esto permite hacer operaciones tales como decosntrucción y manejo de casos exhaustivos, lo que mejora la calidad, legibilidad y predictivibilidad del codigo
al tiempo que se disminuye bastante el "boilerplate" por el que java es famoso.
 */
void main(){
    var person = new Person("Ewig Luftenglanz",31);
    //1. El pattern matching nos permite evaluar de una manera eficiente si una clase es de una isntancia particular
    System.out.println("1)");
    if(person instanceof Person) System.out.println("Es persona");
    //2. El pattern matching nos permite crear "un clon" del objeto de manera transparente y usar este clon para hacer operaciones y transformaciones
    //Esto es bastante positivo ya que una de las caracteristicas más importantes en la "programación orientada a datos" es la inmutabilidad e integridad de la información.
    System.out.println("2)");
    if(person instanceof Person p){
        System.out.println("Sujeto: "+p.age()+", edad: "+p.name());
    }
    //3) El pattern matching tambien nos permite extraer los campos de un un objeto "al vuelo" lo que mejora el rendimiento
    // de la aplicación al usar datos que se almacenan en el stack y no en la heap, de manera más corta que de manera tradicional
    // al tiempo que permite ahcer compobaciones de "saneamiento" y seguridad. A continuación se muestra una comparación entre ambas formas.
    //A esto se le conoce como decosntrucción y es una caracteristica que aun esta en desarrollo. Otros lenguajes como Kotlin y Javascript
    // permiten hacer deconstrucción sin necesidad de una comprobación previa
    // Tradicional
    int edad1;
    String nombre1;
    System.out.println("3)");
    System.out.println("Tradicional");
    if(person instanceof Person){
        edad1 = person.age();
        nombre1 = person.name();
        System.out.println(edad1+", "+nombre1);
    }
    //Con Pattern matching y deconstrucción
    // notar que se declaran y extraen las variables dentro de Person, además permiten usar
    //Var en su declaración, por lo que es más rapido escribir codigo sin tener que fijarse en la documentación y los tipos de los campos
    System.out.println("pattern matching");
    if(person instanceof Person(var name, var age)) System.out.println(name+", "+age);


    //4. El pattern matching tambien puede usarse en  expresiones switch-case
    System.out.println("4)");
    String name2;
    int age2;
    switch (person){
        case Person(var myName, var myAge) -> {
            name2=myName;
            age2=myAge;
        }
    }
    System.out.println(name2+", "+age2);


    //5. Algo muy util del pattern matching es que es anidado y es cuando más útil es, podemos decosntruir y extraer caracteristicas de objetos dentro de otros y crear o extraer copias "al vuelo"
    System.out.println("5)");
    var cube = new Cube(new Square(10,20),30);
    if(cube instanceof Cube(Square(var x,var y),var z)){
        System.out.println("X: "+x+", Y: "+y+", Z: "+z);

    }
    //Alternativamente puede hacerse de manera separada
    if(cube instanceof Cube(var square,var z) && square instanceof Square(var x, var y)){
        System.out.println("X: "+x+", Y: "+y+", Z: "+z);

    }



}
private record Person(String name, int age){}
private record Square(int lengthX, int lengthY){}
private record Cube(Square square, int lengthZ){}