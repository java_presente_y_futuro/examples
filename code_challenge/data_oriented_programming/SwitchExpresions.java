import java.util.ArrayList;
import java.util.List;

void main(){
    var listOfPeople = List.of(
            new Employee("Daniel",28,350.8),
            new User("Ewig",31),
            new Employee("Alejandra",36,481.8),
            new User("Roonie", 61)
    );
    listOfPeople.forEach(p ->{
        assignValuePerCase(p);
        separateUsersFromEmployees(p);
        turnUsersIntoEmployees(p);
        switchPatternMatching(p);
    });

}
//1 Al switch ser una expresión el resultado puede ser asignado a un valor;
private static void assignValuePerCase(Person person){
    var res = switch (person) {
            case Employee employee -> "1";
            case User user -> 2;
    };
    System.out.println(res);
}
//2 Pueden hacerse operaciones exhaustivas (No requeire default
private static List<User> separateUsersFromEmployees(Person person){
    var userList = new ArrayList<User>();
    var employeeList = new ArrayList<Employee>();

    switch (person){
        case Employee employee ->employeeList.add(employee);
        case User user -> userList.add(user);
    }
    return userList;
}
//3 puede usarse como campo para crear un nuevo objeto, en este caso objetos String
private static void turnUsersIntoEmployees(Person person){
    var names = new ArrayList<String>();
    names.add(switch (person) {
        case Employee employee -> "Empleado";
        case User user -> "Usuario";
    });
    System.out.println(names);
}
//4 Finalmente Switch soporta pattern matching
private static void switchPatternMatching(Person person){
    switch (person){
        case Employee (var name, var age, var salary) ->
                System.out.println(STR."""
                    Empleado:
                        \tname: \{name},
                        \tage: \{age},
                        \tsalary: \{salary}
                    """);
        case User (var name, var age) ->
                System.out.println(STR."""
                    Usuario:
                        \tname: \{name},
                        \tage: \{age}
                """);
        }
}
private  record User(String name, int age) implements Person{}
private  record Employee(String name, int age, double salary) implements Person{}
private sealed interface Person permits User, Employee{}
