import javax.xml.transform.Source;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

void main() throws IOException {
    /*
    Normalmente para leer de la consola se usan 2 emétodos usando la clase Scanner o la clase StreamBuffer
    De estas 2 la más recomendable, ya que no cuenta con los problemas de comportamiento inconsistente
    por residuos en el buffer del buffer de datos (Por ejemplo nextInt), es usar BufferedReader junto con InputStreamReader,
    sin embargo su uso es tedioso por lo largo que resulta escribirlo.
     */
    var inputBuffer = new BufferedReader(new InputStreamReader(System.in));// notar lo largo y tedioso que es escribir esto
    try {// además requeire de un manejo especial con try catch
        System.out.println("Por favor entre un dato");
        var firstInput = inputBuffer.readLine();
        System.out.println(firstInput);
    } catch (IOException e) {
        throw new RuntimeException(e);
    }
    // En general puede verse que algo tan sencillo como leer datos de la consola, que en otros lenguajes es una operación
    // trivial de invocar una función (input) en Java es bastante más complejo.

    /*
    Existe una alternativa que ya lidia con todas estas cosas debajo de la mesa, pero su utilidad es más limitada y no puede
    usarse en aplicaciones que no tengan un buffer directo con la terminal (por ejemplo aplicaciones ejecutadas
    automaticamente al inicio del sistema mediante systemd en linux o procesos/hilos en modo daemon)
    sin embargo para programas sencillos de consola es suficiente. La clase Console
     */
    var console = System.console(); // Se crea un objeto console
    //Se usa el metodo readLine. por favor notar que se puede poner un mensaje directamente
    var consoleInput = console.readLine("Por favor introdusca un input: ");
    // Se le da manejo al mensaje. Tener en cuenta que console.readLine devuelve es un String.
    System.out.println(consoleInput);
    // Creo que console es más sencilla y facil de entender que las otras alternativas ya que su comportamiento es más similar
    //al visto en otros lenguajes como python (input), o C# (System.console.readLine()), o C/C++ (scanf, gets, cin)
    //sobretodo para estudiantes.
}