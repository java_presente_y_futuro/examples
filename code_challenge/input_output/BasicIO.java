import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;

void main() throws IOException {
    classicFilesReader();
    System.out.println("////////////////");
    modernFilesReader();
    //classifFileWritter("hola mundo");
    modernFileWritter("En algún lugar de la mancha");
}
//1) procesamiento clasico de archivos en java
private static void classicFilesReader()  {
    /*A) Tradicionalmente para leer archivos de texto en java se usa un consepto muy similar al que usaba
    C en su momento: leer un stream de datos, almacenarlos en un buffer y luego procesar los datos del buffer según convenga
     */
    try(BufferedReader bf = new BufferedReader(new FileReader("/home/operador/Documentos/java presente y futuro/examples/code_challenge/input_output/lorem_ipsum.txt"))){
        String line;
        while( (line = bf.readLine())!=null){
            System.out.println(line);
            // más procesamientpo si es requerido
        }


    } catch (IOException e) {
        throw new RuntimeException(e);
    }
    //B) El problema con este enfoque es que es demasiado largo y poco intuitivo, además el hecho de que FileReader reciba
    //un String puede ser problematico debido a que hay particularidades en el manejo de rutas de un sistema operativo al otro
    // por lo que pueden aparecer bugs dificiles de inferir para novatos.

    //Adicional a esto este metodo solo permite leer contenido, no permite hacer operaciones más complejas o distintas.
}

private static void modernFilesReader() throws IOException {
    /* Hoy en día java cuenta con la clase utilitaria Files, perteneciente al paquete java.nio (new input output)
    esta libreria cuenta con gran cantidad de metodos utilitarios para hacer operaciones sobre archivos, incluyendo
    el leer no desde un archivo directamente, sino desde una copia virtual que hace las operaciones de lectura y escritura
    mucho más eficientes.
     */

    var path = Path.of("/home/operador/Documentos/java presente y futuro/examples/code_challenge/input_output/lorem_ipsum.txt");
    var text =  Files.readAllLines(path);
    System.out.println(text);
    // Files no solo permite leer y escribir archivos, tambien permite hacer otras cosas como comprobar si el archivo existe
    // si es un directorio, leer metadata, etc
    if(Files.exists(path)) System.out.println("El archivo existe");
    if(Files.isDirectory(path)) System.out.println("Es un directorio");
    var attributes = Files.readAttributes(path, BasicFileAttributes.class);
    System.out.println(STR."""
            Last accessed time: \{attributes.lastAccessTime()}
            Size of file: \{attributes.size()} bytes
            """);

}

private static void classifFileWritter(String input){
    /*
    De manera analoga existe BufferedWritter. El sus problematicas son analigas a BufferedREader
    es decir se limita a escribir contenido en el archivo, no se pueden hacer más operaciones
    obligan a crear una clase antes de poder usar los metodos de la misma, y además se deben escribir os saltos de linea de manera manual, o usar el metodo newLine() para lo mismo
    además obliga a usar otra clase (File) para comprobar la existencia del archivo antes de poder ejecutar nada
     */
    var newFile = "/home/operador/Documentos/java presente y futuro/examples/code_challenge/input_output/writting.txt";
    try(BufferedWriter writer = new BufferedWriter(new FileWriter(newFile))){
        File file = new File(newFile);
        if(!file.exists()) {
            file.createNewFile();
        }
        writer.write(input);

    } catch (IOException e) {
        throw new RuntimeException(e);
    }
}

private static void modernFileWritter(String input){
    var path = Path.of("/home/operador/Documentos/java presente y futuro/examples/code_challenge/input_output/lorem_ipsum.txt");
    if(!Files.exists(path)) {
        try {
            Files.createFile(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    try {
        Files.writeString(path,input); // Esta no es la unica opción, tambien pueden escribirse archivos de datos no estrcuturados (musica, video, blobs, etc) mediante otros metodos utiles.
    } catch (IOException e) {
        throw new RuntimeException(e);
    }
}


