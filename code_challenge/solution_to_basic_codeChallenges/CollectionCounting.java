import java.util.Arrays;
import java.util.HashMap;/*
Un tipico problema de entrevista laboral consiste en determinar si una palabra A puede ser escrita con las letras de otra
palabra B, es decir B contiene a A. la solución "más correcta" de este algoritmo consiste en contar las veces que cada
letra aparece en cada palabra, si las letras de la palabra evaluada aparecen menos veces que en la palabra que debería
contenerla entonces se dice que la palabra B contiene a A.
 */
import java.util.stream.Collectors;
private static boolean classicSolution(String strA, String strB){
    // en la solución classica lo que se hace es descomponer cada palabra en sus caracteres y
    // ubicarlos en un hashMap como llave, incrementando un cotador que sería el valor.
    var dicA = new HashMap<Character, Integer>();// Se crean los diccionarios vacios
    var dicB = new HashMap<Character, Integer>();
    for(var c: strA.toCharArray()){
        dicA.put(c,dicA.getOrDefault(c,0)+1); // Se hace el conteo de caracteres para el diccionario A
    }
    for (var c:strB.toCharArray()){
        dicB.put(c,dicB.getOrDefault(c,0)+1); // Se repite para strB
    }
    for(var entry: dicA.entrySet()){
        if(dicA.get(entry.getKey())>dicB.get(entry.getKey())) return false; // comparar si el conteo de cada caracter en strA es inferior al de strB
    }
    return true;
}
//La solución anterior es valida. pero hay una manera de escribirlo mucho más corto y declarativo usando programación funcional
private static boolean streamSolution(String strA, String strB){
    // Para ello vamos a usar los "Collectors" podemos obtener un hashMap con el conteo con una sola linea de codigo.
    var dicA = Arrays.stream(strA.split("")).collect(Collectors.groupingBy(c ->c,Collectors.counting())); //extraer conteo de caracteres usando streams
    var dicB = Arrays.stream(strB.split("")).collect(Collectors.groupingBy(c ->c,Collectors.counting()));
    for(var entry: dicA.entrySet()){
        if(dicA.get(entry.getKey())>dicB.get(entry.getKey())) return false; // comparar si el conteo de cada caracter en strA es inferior al de strB
    }
    return true;
}
void main(){
    var classicTrue = classicSolution("amor","aamor");
    var classicFalse = classicSolution("aamor","amor");
    var streamTrue = streamSolution("amor","aamor");
    var streamFalse = streamSolution("aamor","amor");
    System.out.println(STR."""
    Resultados:
            \tClassic true: \{classicTrue}
            \tClassic false: \{classicFalse}
            \tStreamTrue: \{streamTrue}
            \tStreamFalse: \{streamFalse}
            """);

}
