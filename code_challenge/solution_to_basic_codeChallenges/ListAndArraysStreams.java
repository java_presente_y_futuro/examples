import java.util.Arrays;/*
Dado un arreglo de numeros, eliminar los duplicados y regresar un nuevo arreglo además del numero total de
elementos que quedarón
 */

private static RemovingSolutions removeDuplicates(int[] arr){
    var newArr = Arrays.stream(arr).distinct().toArray();
    return new RemovingSolutions(newArr.length,newArr);
}

/*
Dado un array de enteros y un elemento objetivo, eliminar todas las ocurrencias de dicho objetivo del array
 */
private static RemovingSolutions removingElement(int target, int[] arr){
    var res = Arrays.stream(arr).filter(n -> n!=target).toArray();
    return new RemovingSolutions(res.length,res);
}

/*
Devolver el numero de caracteres de la ultima palabra de una cadena de caracteres
 */
private static int lengthOfLastWord(String phrase){
    return Arrays.stream(phrase.trim().split(" ")).toList().getLast().split("").length;
}

void main(){

    // Reto 1
    var arr1 = new int[]{0,0,1,1,1,2,2,3,3,4};
    var removeDuplicates =  removeDuplicates(arr1);
    var count = removeDuplicates.count();
    var arrRes = removeDuplicates.arr();
    System.out.println(STR."""
            length: \{count}
            res: \{Arrays.toString(arrRes)}
            """);
    // Reto 2
    var arr2 = new int[]{0,1,2,2,3,0,4,2};
    var target = 2;
    var removedElements = removingElement(target,arr2);
    System.out.println(STR."""
            length: \{removedElements.count()}
            res: \{Arrays.toString(removedElements.arr())}
            """);
    // reto 3
    var phrase1 = "Hello world";
    var phrase2 = "   fly me   to   the moon  ";
    var phrase3 = "luffy is still joyboy";
    System.out.println(lengthOfLastWord(phrase1));
    System.out.println(lengthOfLastWord(phrase2));
    System.out.println(lengthOfLastWord(phrase3));



}
private record RemovingSolutions(int count, int[] arr){
}